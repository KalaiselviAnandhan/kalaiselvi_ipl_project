IPL PROJECT - 1

SOURCE DATA
    deliveries.csv
    matches.csv


PROBLEMS
   1. Find the number of times each team won the toss and also won the match
   2. Find a player who has won the highest number of Player of the Match awards for each  season
   3. Find the strike rate of a batsman for each season
   4. Find the highest number of times one player has been dismissed by another player
   5. Find the bowler with the best economy in super overs


Problem 1: Number of times each team won the toss and also won the match
    sub-problem 1: Transform the Source Data (deliveries.csv/matches.csv) into JSON format.
    sub-problem 2: Count the number of times each team Won both the toss and the match.

Problem 2: Most Player of the Match Awards per Season
    sub-problem 1: Count the Player of the Match Awards based on the Player and also based on the Season.
    sub-problem 2: Filter Out the Players who has highest Number of Player of the Match Awards per Season.

Problem 3: Strike Rate of a batsman for each Season.
    sub-problem 1: Filter out the ids based on the Season in matches JSON data.
    sub-problem 2: Find Out the Batsman total Runs and total balls Faced.
    sub-problem 3: Calculate the Strike Rate using the formula
                    (Runs Scored / Balls faced)*100

Problem 4: Highest Number of Times one Player has been Dismissed by another Player.
    sub-problem 1: Count the Players dismissed by another Player based on the Dismissal Kind.
    sub-problem 2: Filter Out the highest number of times one player has been dismissed by another player.

Problem 5: Bowler with Best Economy in Super Overs.
    sub-problem 1: Find out the total balls throwed by the bowlers and also the total Runs (Without Extra Runs) Conceded in Super Overs.
    sub-problem 2: Calculate the Economy using the formula
                    Runs Conceded / (Total Balls / 6)
    sub-problem 3: Find out the minimal Economy Rate.
