const csv = require("csvtojson");
const fs = require("fs");

const ipl = require("./ipl.js");

const matchesFilePath = "../data/matches.csv";
const deliveriesFilePath = "../data/deliveries.csv";

const teamWonTossAndMatchFilePath = "../output/teamWonTossandMatch.json";
const mostPlayerOfTheMatchPerSeasonFilePath = "../output/mostPlayerOfTheMatchPerSeason.json";
const batsmanStrikeRateFilePath = "../output/BatsmanStrikeRate.json";
const playerDismissedFilePath = "../output/PlayerDismissed.json";
const bowlerEconomyFilePath = "../output/BowlerWithBestEconomy.json";

function main(){
    csv({colParser:{"id":"number"}})
    .fromFile(matchesFilePath)
    .then((matchesJsonObj)=>{

        csv({colParser:{"match_id":"number","batsman_runs":"number"}})
        .fromFile(deliveriesFilePath)
        .then((deliveriesJsonObj) => {
            let teamWonTossAndMatch = ipl.teamWonBothTossandMatch(matchesJsonObj);
            let mostPlayerOfTheMatchPerSeason = ipl.mostPlayerOfTheMatchPerSeason(matchesJsonObj);
            let batsmanStrikeRate = ipl.batsmanStrikeRate(matchesJsonObj,deliveriesJsonObj);
            let playersDismissed = ipl.playerDismissed(deliveriesJsonObj);
            let bowlerwithBestEconomy = ipl.bowlerWithBestEconomy(deliveriesJsonObj);

            saveOutput(mostPlayerOfTheMatchPerSeason, mostPlayerOfTheMatchPerSeasonFilePath)
            .then(() => saveOutput(teamWonTossAndMatch, teamWonTossAndMatchFilePath))
            .then(() => saveOutput(batsmanStrikeRate, batsmanStrikeRateFilePath))
            .then(() => saveOutput(playersDismissed, playerDismissedFilePath))
            .then(() => saveOutput(bowlerwithBestEconomy,bowlerEconomyFilePath))
            .catch((err) => {
                console.log(err);
            })
        });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    });
}

function saveOutput(result,outputFilePath){
    const jsonString = JSON.stringify(result, null, 2);
    return new Promise((resolve,reject)=>{
        fs.writeFile(outputFilePath, jsonString, "utf-8", (err)=>{
            if(err){
                console.log("Error: while Writing a Output into a json file");
                reject(err);
            }
            else{
                resolve();
            }
        });
    })
}
main();