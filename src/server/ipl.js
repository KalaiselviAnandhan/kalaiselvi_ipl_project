exports.teamWonBothTossandMatch = require("./IPL/teamWonBothTossandMatch.js").teamWonBothTossandMatch

exports.mostPlayerOfTheMatchPerSeason = require("./IPL/mostPlayerOfTheMatchPerSeason.js").mostPlayerOfTheMatchPerSeason

exports.batsmanStrikeRate = require("./IPL/batsmanStrikeRate.js").batsmanStrikeRate

exports.playerDismissed = require("./IPL/playerDismissed.js").playerDismissed

exports.bowlerWithBestEconomy = require("./IPL/bowlerWithBestEconomy.js").bowlerWithBestEconomy