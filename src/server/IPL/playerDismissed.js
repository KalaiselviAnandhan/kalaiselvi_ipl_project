exports.playerDismissed = (deliveries) => {
    let playersDismissed = {};
    for(let row of deliveries){
      if(row["player_dismissed"] != "" && row["dismissal_kind"] != "lbw" && row["dismissal_kind"] != "hit wicket" && row["dismissal_kind"] != "retired hurt" && row["dismissal_kind"] != "obstructing the field"){
        playersDismissed[row["player_dismissed"]] = playersDismissed.hasOwnProperty(row["player_dismissed"]) ? playersDismissed[row["player_dismissed"]] + 1 : 1;
      }
    }
    let values = Object.values(playersDismissed);
    let maximaldismiss = Math.max(...values);
    let keys = Object.keys(playersDismissed);
    let index = values.indexOf(maximaldismiss);
    let player = {};
    player[keys[index]] = maximaldismiss; 
    return  player;
  }