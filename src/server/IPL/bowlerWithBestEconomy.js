exports.bowlerWithBestEconomy = (deliveries) => {
    let bowlerEconomy = {}, bowlerEconomyinSuperOver = {};
    for(let row of deliveries){
      if(row["is_super_over"] == 1){
      if(!bowlerEconomy.hasOwnProperty(row["bowler"])){
        bowlerEconomy[row["bowler"]] = {};
      }
      bowlerEconomy[row["bowler"]]["balls"] = bowlerEconomy[row["bowler"]].hasOwnProperty("balls") ? bowlerEconomy[row["bowler"]]["balls"] + 1 : 1;
      bowlerEconomy[row["bowler"]]["runs"] = bowlerEconomy[row["bowler"]].hasOwnProperty("runs") ? bowlerEconomy[row["bowler"]]["runs"] + (row["total_runs"] - row["extra_runs"]) : (row["total_runs"] - row["extra_runs"]);
    }
    }
  
    for(let bowler in bowlerEconomy){
        bowlerEconomyinSuperOver[bowler] = (bowlerEconomy[bowler].runs) / (bowlerEconomy[bowler].balls / 6);
    }
    let values = Object.values(bowlerEconomyinSuperOver);
    let keys = Object.keys(bowlerEconomyinSuperOver);
    let minimal = Math.min(...values);
    let index = values.indexOf(minimal);
    let bowler = {};
    bowler[keys[index]] = minimal;
    return bowler;
}