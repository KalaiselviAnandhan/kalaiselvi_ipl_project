exports.batsmanStrikeRate = (matches,deliveries)=>{
    let strikeRate = {}, seasonids = {};
  
    matches.forEach((obj)=>{
      if(!strikeRate.hasOwnProperty(obj["season"])){
        strikeRate[obj["season"]] = {}
        seasonids[obj["season"]] = [];
      }
  
      if(seasonids.hasOwnProperty(obj["season"])){
        seasonids[obj["season"]].push(obj["id"])  
      }  
    })

    let callback = function (ids,deliveries){
      let batsmaninfo = {};
      for(let obj of deliveries){
        if(ids.includes(obj["match_id"])){
          if(!batsmaninfo.hasOwnProperty(obj["batsman"])){
            batsmaninfo[obj["batsman"]] = {};
          }
          batsmaninfo[obj["batsman"]]["ballsfaced"] = batsmaninfo[obj["batsman"]].hasOwnProperty("ballsfaced") ? batsmaninfo[obj["batsman"]]["ballsfaced"] + 1 : 1;
          batsmaninfo[obj["batsman"]]["runsScored"] = batsmaninfo[obj["batsman"]].hasOwnProperty("runsScored") ? batsmaninfo[obj["batsman"]]["runsScored"] + obj["batsman_runs"] : obj["batsman_runs"];      
        }
      }
      return batsmaninfo;
    }
    let findStrikeRate = function (cb,ids,deliveries){
      let result = {};
      let batsmaninfo = cb(ids,deliveries);
      for(let value in batsmaninfo){
        result[value] = Number(((batsmaninfo[value].runsScored / batsmaninfo[value].ballsfaced)*100).toFixed(2));
      }
      return result; 
    }
    for(let year in seasonids){
      strikeRate[year] = findStrikeRate(callback,seasonids[year],deliveries);
    }
      
    return strikeRate;
  }