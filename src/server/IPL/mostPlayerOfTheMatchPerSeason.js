exports.mostPlayerOfTheMatchPerSeason = (matches)=>{
    let playerOftheMatchperYear = {};
    let mostPlayerOfTheMatchPerYear = {};
    for(let obj of matches){
      if(!playerOftheMatchperYear.hasOwnProperty(obj["season"])){
        playerOftheMatchperYear[obj["season"]] = {};
      }
      playerOftheMatchperYear[obj["season"]][obj["player_of_match"]] = playerOftheMatchperYear[obj["season"]].hasOwnProperty(obj["player_of_match"])?playerOftheMatchperYear[obj["season"]][obj["player_of_match"]]+1:1;
    }
    for(let season in playerOftheMatchperYear){
      let players = Object.keys(playerOftheMatchperYear[season]);
      let counts = Object.values(playerOftheMatchperYear[season]);
      let maxcount = Math.max(...counts);
      let index = counts.indexOf(maxcount);
      mostPlayerOfTheMatchPerYear[season] = [players[index],maxcount];
    }
    return mostPlayerOfTheMatchPerYear;
}