exports.teamWonBothTossandMatch = (matches) => {
    let teamWon = {};
    for(let obj of matches){
      if(obj["toss_winner"] === obj["winner"]){
        teamWon[obj["winner"]] = teamWon[obj["winner"]]?teamWon[obj["winner"]] + 1 : 1;
      }
    }
    return teamWon;
}